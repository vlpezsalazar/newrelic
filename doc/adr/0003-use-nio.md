# 3. Use NIO to enable reactive processing.

Date: 2021-03-24

## Status

Accepted

## Context

The specs requires to maximize the throughput of the application without using any
typical framework for web development like akka, spring webflux or micronaut that 
provides reactive processing out of the box. Context switching is expensive and using
blocking connections is left out of the picture to reach the required performance.

## Decision

[Use NIO (look for NumberProcessingTask.](../../src/main/java/countnumbers/CounterService.java#).

## Consequences

Selectable non blocking socket channels allows to provide reactive processing without 
actually increasing the threads required to manage connections. A single thread is 
enough to deal with 5 concurrent clients at a time. 