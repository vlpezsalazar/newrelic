# 2. Use Spock for Testing.

Date: 2021-03-21

## Status

Accepted

## Context

We need a method that enables us to develop following the specs.

## Decision

Use groovy and Spock.

## Consequences

A given-when-then style makes it easier to adopt TDD and more explicit what is tested.