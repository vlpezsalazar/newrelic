# 4. Keep separate memory lines.

Date: 2021-03-26

## Status

Accepted as an improvement, but not required for the current specs.

## Context

If parallelism is required for those cases where the specs changes from 5 to a much higher number of connections 
the processing of the numbers would became heavy and using the concurrent versions of Collections to store the 
numbers can be problematic due to contention, or expensive if the CopyOnWrite is used.


## Decision

To avoid the use of specialised concurrent collections to store numbers in case of multiple threads processing inputs 
at the same time we use as much ["memory lines"](../../src/main/java/countnumbers/Storage.java) as threads.

## Consequences

Reduced contention and there is no need to use synchronised versions of stores.