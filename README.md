# Count Numbers Service

This is an asynchronous non blocking server that provides the simple functionality of counting
numbers sent by clients on port 4000 and printing a report each 10 seconds with a count of 
unique and repeated numbers.

## How to build the application

The application uses gradle to build and test. Use the following commands to:

  - Build: ./gradlew build -> compiles and run the tests.
  - Create a jar: ./gradlew jar -> creates a runnable newrelic-1.0.jar file in build/libs.
  - Run the application: ./gradlew run -> starts the server in http://localhost:4000


## Architecture Decision Records

* [1. Record architecture decisions.](doc/adr/0001-record-architecture-decisions.md)
* [2. Use Spock for Testing.](doc/adr/0002-use-groovy-for-testing.md)
* [3. Use NIO to enable reactive processing.](doc/adr/0003-use-nio.md)
* [4. Keep separate memory lines.](doc/adr/0004-keep-separate-memory-lines.md)