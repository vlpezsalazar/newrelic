package countnumbers

import spock.lang.Specification

import java.nio.ByteBuffer
import java.nio.channels.SocketChannel
import java.nio.file.Files
import java.nio.file.Path

class CounterServiceSpec extends Specification {

    //CounterServiceControl
//1. The Application must accept input from at most 5 concurrent clients on TCP/IP port 4000.

    public static final String LOG_FILE_NAME = "numbers.log"

    void 'should print non repeated numbers in the log file'() {
        given:
        def connectionPort = 4000
        def (
        CounterService      server,
        List<Thread>        clientTasks
        ) = stateIs(
                numbers: [
                        "111111111${System.lineSeparator()}555555555${System.lineSeparator()}",
                        "222222222${System.lineSeparator()}222222222${System.lineSeparator()}",
                        "333333333${System.lineSeparator()}",
                ],
                clientIterations: 1,
                maxNumberOfClients: 3,
                connectionPort: connectionPort
        )

        when:
        clientTasks.each { clientTask ->
            clientTask.start()
        }
        Thread.sleep(12_000)

        and:
        def numbers = Files.readString(Path.of(LOG_FILE_NAME))

        then:
        verifyAll {
            numbers.count("111111111${System.lineSeparator()}") == 1
            numbers.count("222222222${System.lineSeparator()}") == 1
            numbers.count("333333333${System.lineSeparator()}") == 1
            numbers.count("555555555${System.lineSeparator()}") == 1
        }


        cleanup:
        server.stop()

    }

    private List stateIs(LinkedHashMap<String, ?> params) {
        stateIs(
                (params.numbers ?: []) as List<?>,
                params.connectionPort as Integer,
                (params.maxNumberOfClients ?: 5) as Integer,
                params.clientIterations as Integer

        )
    }

    private List stateIs(List<?> lines, int connectionPort, int maxNumberOfClients, int clientIterations) {
        final clientTasks = []

        final storage = new Storage.DefaultStorage(maxNumberOfClients)
        final counterService = new CounterService(connectionPort, new InputHandler.DefaultInputHandler(), storage)
        counterService.start(maxNumberOfClients, new ReportingTask(LOG_FILE_NAME, storage, new Counter.DefaultCounter()))

        lines.each { line ->
            def client = new NumbersClient(connectionPort)
            clientTasks.add(new Thread(new Runnable() {
                @Override
                void run() {
                    clientIterations.times {
                        if (line instanceof Closure<String>) {
                            client.sendLine(line())
                        } else {
                            client.sendLine(line.toString())
                        }

                    }
                }
            }))
        }

        [counterService, clientTasks]

    }

//9. If any connected client writes a single line with only the word "terminate" followed by a server-native newline sequence, the Application must disconnect all clients and perform a clean shutdown as quickly as possible.
//7. Any data that does not conform to a valid line of input should be discarded and the client connection terminated immediately and without comment.


    class NumbersClient implements Closeable {

        private SocketChannel channel

        NumbersClient(int connectionPort) {
            this.channel = SocketChannel.open(new InetSocketAddress("localhost", connectionPort))
        }

        void sendLine(String msg) throws IOException {
            ByteBuffer buffer = ByteBuffer.wrap(msg.getBytes())
            channel.write(buffer)
        }

        @Override
        void close() {
            channel.close()
        }
    }
}
