package countnumbers

import spock.lang.Specification

import java.util.function.IntSupplier
import java.util.stream.IntStream

import static countnumbers.Counter.*

//LogManagement
//4. The log file, to be named "numbers.log”, must be created anew and/or cleared when the Application starts.
//5. Only numbers may be written to the log file. Each number must be followed by a server-native newline sequence.
//6. No duplicate numbers may be written to the log file.

//Report
//8. Every 10 seconds, the Application must print a report to standard output:
class CounterSpec extends Specification{

    //CountLogic
    //3. The total number of unique numbers received for this run of the Application. 4. Example text for #8: Received 50 unique numbers, 2 duplicates. Unique total: 567231
    void 'should count the total number of unique numbers received for this run of the Application'(){
        given:
        def numberOfRounds = aNumberBetween(1, 5)
        def uniqueNumbersExpected = aNumberBetween(numberOfRounds, 1_000_000)

        def (
        Counter sut,
                               _,
        IntStream              stream
        ) = stateIs(
                numberOfRounds: numberOfRounds - 1,
                uniqueNumbersToGenerate: uniqueNumbersExpected,
                repeatedNumbersToGenerate: aNumberBetween(0, 20_000)
        )

        expect:
        sut.statistics(stream).totalCountOfUniqueNumbers == uniqueNumbersExpected
    }

    //1. The difference since the last report of the count of new unique numbers that have been received.
    void 'should count the difference since the last report of the new unique numbers that have been received'(){

        given:
        int numberOfRounds = aNumberBetween(2, 10)
        int uniqueNumbersGenerated = aNumberBetween(numberOfRounds, 50)
        def (
        Counter      sut,
        List<Report> reports,
        IntStream    stream
        ) = stateIs(
                numberOfRounds: numberOfRounds - 1,
                numberOfElementsPerRound: Math.floorDiv(uniqueNumbersGenerated, numberOfRounds),
                uniqueNumbersToGenerate: uniqueNumbersGenerated,
                repeatedNumbersToGenerate: aNumberBetween(0, 20)

        )

        expect:
        sut.statistics(stream).uniqueNumbersSinceLastReport.size() as Long == uniqueNumbersGenerated - reports[Math.max(0, numberOfRounds - 2)].totalCountOfUniqueNumbers

    }


    //2. The difference since the last report of the count of new duplicate numbers that have been received.
    void 'should count the difference since the last report of new duplicate numbers that have been received'(){
        given:
        int numberOfRounds = aNumberBetween(2, 10)
        int uniqueNumbersGenerated = aNumberBetween(numberOfRounds, 50)
        def repeatedNumbersToGenerate = aNumberBetween(5, 20)
        def (
        Counter      sut,
        List<Report> reports,
        IntStream    stream
        ) = stateIs(
                numberOfRounds: numberOfRounds - 1,
                numberOfElementsPerRound: distributeEvenly(uniqueNumbersGenerated, numberOfRounds),
                uniqueNumbersToGenerate: uniqueNumbersGenerated,
                repeatedNumbersToGenerate: repeatedNumbersToGenerate

        )

        expect:
        sut.statistics(stream).countOfRepeatedNumbersSinceLastReport == repeatedNumbersToGenerate - (reports.sum{ it.countOfRepeatedNumbersSinceLastReport } as Long)
    }

    private static List stateIs(Map<String, Integer> params) {

        def uniqueNumbersToGenerate = params.uniqueNumbersToGenerate?: 1
        def repeatedNumbersToGenerate = params.repeatedNumbersToGenerate?: 0
        def totalNumbersToGenerate = uniqueNumbersToGenerate + repeatedNumbersToGenerate
        def numberOfRounds = params.numberOfRounds?: 0
        def numberOfElementsPerRound = params.numberOfElementsPerRound?: distributeEvenly(totalNumbersToGenerate, numberOfRounds)

        stateIs(
                numberOfRounds,
                numberOfElementsPerRound,
                uniqueNumbersToGenerate,
                repeatedNumbersToGenerate
        )
    }


    private static int distributeEvenly(int totalNumbersToGenerate, int numberOfRounds) {
        Math.floorDiv(totalNumbersToGenerate, Math.max(numberOfRounds, 1))
    }

    private static int aNumberBetween(int min=0, int max) {
        new Random().nextInt(max) + min
    }

    private static List stateIs(int numberOfRounds, int numberOfElementsPerRound, int uniqueNumbersToGenerate, int repeatedNumbersToGenerate){
        def counter = new DefaultCounter()
        def totalNumbersToGenerate = uniqueNumbersToGenerate + repeatedNumbersToGenerate
        def generator = new IntGenerator(uniqueNumbersToGenerate, repeatedNumbersToGenerate)
        def reports = []

        numberOfRounds.times {
            reports.add(counter.statistics(IntStream.generate(generator).limit(numberOfElementsPerRound)))
        }

        [counter, reports, IntStream.generate(generator).limit(totalNumbersToGenerate - numberOfRounds*numberOfElementsPerRound)]
    }


    private static class IntGenerator implements IntSupplier {

        private final List<Integer> numbers
        private int index = 0

        private IntGenerator(int uniqueNumbersToGenerate, int repeatedNumbersToGenerate){
            def seed = generateInitialRandomNumber()
            def limit = seed + uniqueNumbersToGenerate - 1

            numbers = (seed..limit).step(1)

            repeatedNumbersToGenerate.times {
                numbers.add(numbers[aNumberBetween(0, uniqueNumbersToGenerate)])
            }
            Collections.shuffle(numbers)

        }

        private static int generateInitialRandomNumber() {
            new Random(System.currentTimeMillis()).nextInt(1_000_000_000)
        }

        @Override
        int getAsInt() {
            int number = numbers[index++]

            index %= numbers.size()

            return number
        }
    }
}
