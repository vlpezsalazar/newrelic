package countnumbers

import spock.lang.Specification

import java.nio.ByteBuffer

import static countnumbers.InputHandler.ResultType.*

//InputValidation


class InputHandlerSpec extends Specification {

    //2. Input lines presented to the Application via its socket must either be composed of exactly nine decimal digits (e.g.: 314159265 or 007007009) immediately followed by a server-native newline sequence; or a termination sequence as detailed in #9, below.

    void 'should accept input lines of nine decimal digits and server-native newline sequence;'() {
        given:
        def sut = new InputHandler.DefaultInputHandler()

        expect:
        sut.accept(ByteBuffer.wrap(readFromInput.getBytes())).with {
            verifyAll {
                number == expectedNumber
                type == PROCESS_NUMBER
            }
        }

        where:
        readFromInput                            | expectedNumber
        "012345678${System.lineSeparator()}"     | 12345678
        "987654321${System.lineSeparator()}"     | 987654321
        "984321025${System.lineSeparator()}blah" | 984321025

    }

    void 'Illegal sequences are rejected'() {
        given:
        def sut = new InputHandler.DefaultInputHandler()

        expect:
        sut.accept(ByteBuffer.wrap(readFromInput.getBytes())).with {
            verifyAll {
                type == ILLEGAL_INPUT
            }
        }

        where:
        readFromInput <<
                ["5678${System.lineSeparator()}",
                 "98${System.lineSeparator()}7654321",
                 "ABC",
                 "012345678\r\n",
                 "term012345678${System.lineSeparator()}"]

    }

    void 'should accept a termination sequence'() {
        given:
        InputHandler.DefaultInputHandler sut = new InputHandler.DefaultInputHandler()

        expect:
        sut.accept(ByteBuffer.wrap("terminate${System.lineSeparator()}".getBytes())).with {
            verifyAll {
                type == TERMINATE
            }
        }
    }
}
