package countnumbers;

import lombok.Data;

import java.nio.ByteBuffer;

import static java.lang.Character.isDigit;

public interface InputHandler {

    InputLine accept(ByteBuffer buffer);

    @Data
    class InputLine {
        final int number;
        final ResultType type;

    }

    enum ResultType {
        PROCESS_NUMBER, TERMINATE, ILLEGAL_INPUT
    }

    class DefaultInputHandler implements InputHandler {

        private static final String LINE_SEPARATOR = System.lineSeparator();
        private static final String TERMINATION_LINE = "terminate";
        private static final int MAX_NUMBER_OF_DIGITS = 9;

        @Override
        public InputLine accept(ByteBuffer buffer) {
            int number = 0;
            int numberOfDigits = 0;

            StringBuilder newLineBuilder = new StringBuilder();
            StringBuilder terminationBuilder = new StringBuilder();

            while (buffer.hasRemaining() && !isEndOfElement(newLineBuilder)) {
                char currentChar = (char) (buffer.get() & 0xFF);
                if (isEndOfLine(terminationBuilder, numberOfDigits) && currentChar == '\r') {
                    newLineBuilder.append(currentChar);
                } else if (isEndOfLine(terminationBuilder, numberOfDigits) && currentChar == '\n') {
                    newLineBuilder.append(currentChar);
                } else if (isDigit(currentChar)) {
                    numberOfDigits++;
                    number = number * 10 + currentChar - '0';
                } else if (isTerminationElement(currentChar)){
                    terminationBuilder.append(currentChar);
                } else {
                    break;
                }
            }

            return new InputLine(number, getResultType(numberOfDigits, newLineBuilder, terminationBuilder));
        }

        private ResultType getResultType(int numberOfDigits, StringBuilder newLineBuilder, StringBuilder terminationBuilder) {
            final ResultType result;
            if (isaNumber(numberOfDigits, newLineBuilder, terminationBuilder)) {
                result =  ResultType.PROCESS_NUMBER;
            } else if (isaTerminator(numberOfDigits, newLineBuilder, terminationBuilder)){
                result = ResultType.TERMINATE;
            } else {
                result = ResultType.ILLEGAL_INPUT;
            }

            return result;
        }



        private boolean isaTerminator(int numberOfDigits, StringBuilder newLineBuilder, StringBuilder terminationBuilder) {
            return isEndOfElement(newLineBuilder) && TERMINATION_LINE.contentEquals(terminationBuilder) && numberOfDigits == 0;
        }

        private boolean isaNumber(int numberOfDigits, StringBuilder newLineBuilder, StringBuilder terminationBuilder) {
            return isEndOfElement(newLineBuilder) && MAX_NUMBER_OF_DIGITS == numberOfDigits && terminationBuilder.length() == 0;
        }

        private boolean isEndOfElement(StringBuilder newLineBuilder) {
            return LINE_SEPARATOR.contentEquals(newLineBuilder);
        }

        private boolean isEndOfLine(StringBuilder terminationBuilder, int numberOfDigits) {
            return TERMINATION_LINE.contentEquals(terminationBuilder) || MAX_NUMBER_OF_DIGITS == numberOfDigits;
        }

        private boolean isTerminationElement(char element) {
            return TERMINATION_LINE.chars().anyMatch((integer) -> integer == element);
        }

    }

}
