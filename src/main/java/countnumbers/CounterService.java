package countnumbers;

import countnumbers.InputHandler.InputLine;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Set;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import static countnumbers.InputHandler.ResultType.PROCESS_NUMBER;

@Slf4j
class CounterService {

    private static final String INPUT_PROCESSING = "input_processing";
    private static final String REPORTING = "reporting";

    private final AtomicBoolean shouldProcessInput;
    private final InetSocketAddress socketAddr;
    private final InputHandler handler;
    private final Storage storage;
    private final ScheduledExecutorService reportingExecutorService;
    private final ExecutorService inputProcessingExecutorService;

    CounterService(int port, InputHandler handler, Storage storage) {
        this.socketAddr = new InetSocketAddress("localhost", port);
        this.handler = handler;
        this.storage = storage;
        this.shouldProcessInput = new AtomicBoolean(true);

        this.reportingExecutorService = Executors.newScheduledThreadPool(1,
                new NamedThreadFactory(REPORTING, Executors.defaultThreadFactory())
        );
        this.inputProcessingExecutorService = Executors.newFixedThreadPool( 1,
                new NamedThreadFactory(INPUT_PROCESSING, Executors.defaultThreadFactory())
        );
    }

    void start(int maxConcurrentClients, Runnable reportingTask) {
        System.out.println("Starting server on port " + socketAddr.getPort());
        inputProcessingExecutorService.submit(new NumberProcessingTask(maxConcurrentClients, handler, storage));
        reportingExecutorService.scheduleAtFixedRate(reportingTask, 10, 10, TimeUnit.SECONDS);
    }

    public void stop() {
        System.out.println("Shutdown requested...");
        shouldProcessInput.setRelease(false);
        reportingExecutorService.shutdownNow();
    }

    private class NumberProcessingTask implements Runnable {

        private static final int NUMBER_OF_BYTES_PER_ELEMENT = 10;
        private static final int MAX_ELEMENTS_IN_BUFFER = 16 * NUMBER_OF_BYTES_PER_ELEMENT;
        private final int maxNumberOfClients;
        private final InputHandler handler;
        private final Storage storage;

        NumberProcessingTask(int maxNumberOfClients, InputHandler handler, Storage storage) {
            this.maxNumberOfClients = maxNumberOfClients;
            this.handler = handler;
            this.storage = storage;
        }

        @SneakyThrows
        @Override
        public void run() {
            final Selector selector = Selector.open();
            try (ServerSocketChannel serverSocket = ServerSocketChannel.open()) {
                serverSocket.bind(socketAddr);
                serverSocket.configureBlocking(false);
                serverSocket.register(selector, SelectionKey.OP_ACCEPT);

                AtomicInteger processingThreadMemoryLineIndex = new AtomicInteger();
                while (shouldProcessInput.getAcquire()) {
                    selector.select();
                    Set<SelectionKey> selectedKeys = selector.selectedKeys();
                    selectedKeys
                            .stream()
                            .filter(selectionKey -> selectionKey.isValid() && ( selectionKey.isAcceptable() || selectionKey.isReadable() ))
                            .limit(maxNumberOfClients)
                            .peek(selectionKey -> {
                                ByteBuffer buffer = (ByteBuffer) selectionKey.attachment();
                                if (buffer == null) {
                                    selectionKey.attach(ByteBuffer.allocate(MAX_ELEMENTS_IN_BUFFER));
                                } else {
                                    buffer.clear();
                                }
                            })
                            .forEach((selectionKey) -> {
                                try {
                                    if (selectionKey.isAcceptable()) {
                                        register(selector, serverSocket);
                                    }
                                    if (selectionKey.isReadable()) {
                                        processInput(processingThreadMemoryLineIndex, selectionKey);
                                    }

                                } catch (Throwable e) {
                                    log.error("", e);
                                }
                            });
                    selectedKeys.clear();
                }

            } catch (Throwable e) {
                log.error("", e);
            } finally {
                if (selector != null){
                    selector.select();
                    Set<SelectionKey> selectedKeys = selector.selectedKeys();
                    selectedKeys
                            .stream()
                            .filter(SelectionKey::isValid)
                            .map(SelectionKey::channel)
                    .forEach(selectableChannel -> {
                        try {
                            selectableChannel.close();
                        } catch (IOException ignore) {}
                    });
                    selector.close();
                }
                reportingExecutorService.shutdownNow();
            }

        }

        private void processInput(AtomicInteger processingThreadId, SelectionKey selectionKey) throws IOException {
            SocketChannel client = (SocketChannel) selectionKey.channel();
            var buffer = (ByteBuffer) selectionKey.attachment();
            buffer.limit(client.read(buffer));
            buffer.rewind();
            InputLine inputLine;
            do {
                inputLine = handler.accept(buffer);
                switch (inputLine.type) {
                    case PROCESS_NUMBER:
                        storage.addNumber(inputLine.number, processingThreadId);
                        break;
                    case TERMINATE:
                        shouldProcessInput.setRelease(false);
                        break;
                    case ILLEGAL_INPUT:
                        selectionKey.cancel();
                        break;
                }
            } while (buffer.hasRemaining() && inputLine.type == PROCESS_NUMBER);

        }

        private void register(Selector selector, ServerSocketChannel serverSocket) throws IOException {
            SocketChannel client = serverSocket.accept();
            client.configureBlocking(false);
            client.register(selector, SelectionKey.OP_READ);
        }
    }


    private static class NamedThreadFactory implements ThreadFactory {
        private final ThreadFactory delegate;
        private final String groupName;
        private final AtomicInteger id;

        NamedThreadFactory(String groupName, ThreadFactory factory) {
            this.delegate = factory;
            this.groupName = groupName;
            this.id = new AtomicInteger(-1);
        }

        @Override
        public Thread newThread(Runnable r) {
            final Thread thread = delegate.newThread(r);
            thread.setName(groupName + "_" + id.incrementAndGet());
            return thread;
        }
    }

}
