package countnumbers;

import lombok.Data;
import lombok.Value;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public interface Counter {
    Report statistics(IntStream elementsToCount);

    class DefaultCounter implements Counter {

        private final Set<Integer> uniqueElements;

        DefaultCounter() {
            this.uniqueElements = new HashSet<>(10_000_000);
        }

        @Override
        public Report statistics(IntStream elementsToCount) {
            ElementCounter repeatedInThisRound = new ElementCounter();

            final List<String> uniqueElements = elementsToCount
                    .filter((element) -> {
                        boolean wasDifferent = this.uniqueElements.add(element);
                        if (!wasDifferent) repeatedInThisRound.increment();
                        return wasDifferent;

                    })
                    .mapToObj(String::valueOf)
                    .collect(Collectors.toList());

            return new Report(uniqueElements, this.uniqueElements.size(), repeatedInThisRound.count);
        }

        @Data
        private static class ElementCounter {
            int count = 0;

            void increment(){
                count++;
            }
        }

    }



    @Value
    class Report {
        List<String> uniqueNumbersSinceLastReport;
        long totalCountOfUniqueNumbers;
        long countOfRepeatedNumbersSinceLastReport;
    }
}
