package countnumbers;

import countnumbers.InputHandler.DefaultInputHandler;
import countnumbers.Storage.DefaultStorage;

public class Application {

    static final int PORT = 4000;
    static final int MAX_NUMBER_OF_CONCURRENT_CLIENTS = 5;

    public static void main(String[] args){

        DefaultInputHandler inputHandler = new DefaultInputHandler();
        DefaultStorage storage = new DefaultStorage(MAX_NUMBER_OF_CONCURRENT_CLIENTS);
        Counter counter = new Counter.DefaultCounter();
        CounterService counterService = new CounterService(PORT, inputHandler, storage);
        ReportingTask reportingTask = new ReportingTask("numbers.log", storage, counter);
        counterService.start(MAX_NUMBER_OF_CONCURRENT_CLIENTS, reportingTask);

    }
}
