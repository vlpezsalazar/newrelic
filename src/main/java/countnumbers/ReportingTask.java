package countnumbers;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.List;

@Slf4j
public class ReportingTask implements Runnable {
    private final Storage storage;
    private final Counter counter;
    private final File numbersFile;

    ReportingTask(String logFilePath, Storage storage, Counter counter) {
        this.storage = storage;
        this.counter = counter;
        this.numbersFile = new File(logFilePath);
        this.numbersFile.delete();
    }

    @Override
    public void run() {
        final Counter.Report statistics = counter.statistics(storage.toStream());
        final List<String> uniqueNumbersSinceLastReport = statistics.getUniqueNumbersSinceLastReport();

        try {
            Files.write(numbersFile.toPath(), uniqueNumbersSinceLastReport, StandardOpenOption.CREATE, StandardOpenOption.APPEND, StandardOpenOption.WRITE);
        } catch (IOException e) {
            log.error("", e);
        }


        System.out.println(String.format("Received %d unique numbers, %d duplicates. Unique total: %d",
                uniqueNumbersSinceLastReport.size(),
                statistics.getCountOfRepeatedNumbersSinceLastReport(),
                statistics.getTotalCountOfUniqueNumbers())
        );


    }
}
