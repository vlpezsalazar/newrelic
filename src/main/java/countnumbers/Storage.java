package countnumbers;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;
import java.util.stream.IntStream;

public interface Storage {

    void addNumber(int number, AtomicInteger processingThread);
    IntStream toStream();

    class DefaultStorage implements Storage {
        private IntStream.Builder[] memoryLines;
        private final ReadLock readLock;
        private final WriteLock writeLock;

        DefaultStorage(int numberOfLines) {
            ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
            this.readLock = rwl.readLock();
            this.writeLock = rwl.writeLock();
            this.updateMemoryLines(numberOfLines);
        }

        private IntStream.Builder[] updateMemoryLines(int numberOfLines) {
            writeLock.lock();
            IntStream.Builder[] oldMemoryLines = this.memoryLines;
            try {
                this.memoryLines = new IntStream.Builder[numberOfLines];
                IntStream.range(0, numberOfLines)
                        .forEach(index -> memoryLines[index] = IntStream.builder());
            } finally {
                writeLock.unlock();

            }
            return oldMemoryLines;
        }

        @Override
        public void addNumber(int number, AtomicInteger memoryLineIndex) {
            final IntStream.Builder memoryLine;
            readLock.lock();
            try {
                memoryLine = memoryLines[memoryLineIndex.getAndUpdate(operand -> (operand + 1) % memoryLines.length)];
            } finally {
                readLock.unlock();

            }
            memoryLine.accept(number);

        }

        @Override
        public IntStream toStream() {
            return Arrays.stream(updateMemoryLines(memoryLines.length))
                    .flatMapToInt(IntStream.Builder::build);
        }
    }

}
